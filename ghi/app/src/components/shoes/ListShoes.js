import React, {useState,useEffect} from 'react';
import { Link } from 'react-router-dom';
const ListShoes = () => {
    const [shoes, setShoes] = useState([]);
    const fetchData = async () => {
        const url = "http://localhost:8080/api/shoes/"
        const response = await fetch(url)
        if (response.ok) {
            const data = await response.json()
            setShoes(data.shoes)
        }
    }

    const handleDelete = async (e) => {
        const url = `http://localhost:8080/api/shoes/${e.target.id}/`
        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }
        console.log(url)
        const response = await fetch(url, fetchConfigs)
        const data = await response.json()
        console.log(data)
        setShoes(shoes.filter(shoe => String(shoe.id) !== e.target.id))

    }
    useEffect(() => {
        fetchData()
    }, [])

    return (<>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>List Shoes</h1>
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Bin</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                shoes.map(shoe => {
                                    return (
                                        <tr key={shoe.href}>
                                            <td><Link to={`${shoe.id}`}>{shoe.name}</Link></td>
                                            <td>{shoe.bin}</td>
                                            <td><button onClick={handleDelete} id={shoe.id} className="btn btn-danger">Delete</button></td>
                                        </tr>
                                    )
                                })
                        }
                        </tbody>
                    </table>
                    <Link to="/shoes/new"><button className='btn btn-primary'>Create a shoe</button></Link>
                </div>
            </div>
        </div>

    </>);

}
export default ListShoes;
