import { Link } from 'react-router-dom'
import { useState, useEffect} from 'react'

const HatList = () => {
    const [hats, setHats] = useState([])

    const getData = async () => {
        const resp = await fetch('http://localhost:8090/api/hats/')
        if (resp.ok) {
            const data = await resp.json()
            setHats(data.hats)
        }
    }


    useEffect(()=> {
        getData()
    }, [])

    const handleDelete = async (e) => {
        const url = `http://localhost:8090/api/hats/${e.target.id}`

        const fetchConfigs = {
            method: "Delete",
            headers:{
                "Content-Type": "application/json"
            }
        }

        const resp = await fetch(url, fetchConfigs)
        const data = await resp.json()

        console.log(data)
      

        setHats(hats.filter(hat => String(hat.id) !== e.target.id))
    }
    console.log(hats)
    return( <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">   
                    <h1>List Hats</h1>     
                    <table className="table table-striped">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Location</th>
                            </tr>
                        </thead>

                        <tbody>
                            {
                                hats.map(hat => {
                                    return (
                                    <tr key={hat.href}>
                                        <td><Link to={`/hats/${hat.id}`}>{ hat.name }</Link></td>
                                        <td>{ hat.location }</td>
                                        <td><button onClick={handleDelete} id={hat.id} className="btn btn-danger">Delete</button></td>
                                    </tr>
                                    );
                                })
                            }
                        </tbody>
                    </table>
                    <Link to="/hats/new"><button className='btn btn-primary'>Create a hat</button></Link>
                </div>
            </div>
        </div>
    </>
    );
}

export default HatList;