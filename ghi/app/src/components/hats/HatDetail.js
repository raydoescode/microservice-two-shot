import { useParams, Link } from 'react-router-dom'
import { useState, useEffect} from 'react'

const HatDetail = () => {
    const [hat, setHat] = useState({})
    const { id } = useParams()

    const getData = async () => {
        const resp = await fetch(`http://localhost:8090/api/hats/${id}`)
        if (resp.ok) {
            const data = await resp.json()
            setHat(data)
        }
    }
    useEffect(()=> {
        getData()
    }, [])

    const handleDelete = async () => {
        const url = `http://localhost:8080/api/hats/${id}`
        
        const fetchConfigs = {
            method: "Delete",
            headers: {
                "Content-Type": "application/json"
            }
        }

        const resp = await fetch(url, fetchConfigs)
        const data = await resp.json()
    }

    return( <>
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">   
                    <h1>Hat Details</h1>
                    <h4>Name: <span>{hat.name}</span></h4>
                    <h4>Fabric: <span>{hat.fabric}</span></h4>
                    <h4>Color:  <span>{hat.color}</span></h4>
                    <h4>Image: <span><img className="card-img-top" alt="" src={hat.picture_url}/></span></h4>

                    <Link to='/hats' className="btn btn-primary">Return to Hat List</Link>
                    <button onClick={handleDelete} className="btn btn-danger">Delete Hat</button>
                </div>
            </div>
        </div>
    </>
    );
}

export default HatDetail;
