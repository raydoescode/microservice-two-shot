import { BrowserRouter, Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import ListShoes from "./components/shoes/ListShoes"
import ShoesDetail from './components/shoes/ShoesDetail';
import ShoeForm from './components/shoes/ShoeForm'
import HatList from './components/hats/HatList';
import HatDetail from './components/hats/HatDetail';
import HatForm from './components/hats/HatForm';

function App() {
  return (
    <BrowserRouter>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="shoes">
            <Route index element={<ListShoes />} />
            <Route path=":id" element={<ShoesDetail/>}/>
            <Route path="new" element={<ShoeForm/>}/>
          </Route>
          <Route path="hats">
            <Route index element={<HatList/>} />
            <Route path=":id" element={<HatDetail/>} />
            <Route path="new" element={<HatForm />} />
          </Route>
        </Routes>
      </div>
    </BrowserRouter>
  );
}

export default App;
