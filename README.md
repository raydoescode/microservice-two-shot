# Wardrobify

Team:


* Mico Hernandez - Shoes
* Ray Parker - Hats


## Design
We will use boostrap to style our webpage.
We will create a SPA using react and react routes.
We will design 2 restful API microservices to create and store our Model Data.
We will make backend request with our React front-end.
## Shoes microservice
I will create a shoe model that has the fields:
Name = charField (model name)
Manufacturer = charField
Color = charField
picture_url = urlField
bin = foreignKey(BinVO)

and another model for BinVO:
name = charField
import_href = charField

The BinVO will be updated via the Shoes_poller.py

I will then create the restful API to create,delete and edit shoes.

We will use this API on our frontend to get data and dynamically update our webpage.

## Hats microservice

The hats model will have the properties fabric, stylename, color, pictureurl, and a foreign key for location. The location foreign key will have it's own VO to correspond with the wardrobe microservice.
style_name is refrenced as "name" on the hat model.